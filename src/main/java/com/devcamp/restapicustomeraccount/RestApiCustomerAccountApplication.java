package com.devcamp.restapicustomeraccount;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestApiCustomerAccountApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestApiCustomerAccountApplication.class, args);
	}

}
